# What I Learned

## GitLab Pipelines

There are many pipeline templates. 
We can find these templates [here](https://docs.gitlab.com/ee/ci/examples/index.html#cicd-templates) or within the project, `Navigation Menu > CI/CD > Pipelines` if no pipelines created yet. 

### Jobs & Stages

- A stage can have multiple jobs.
- Jobs are not dependent on each other by default. Stages will be executed in order and a stage will not run if previous stage fails.

```yaml
stages:
  - stage-1
  - stage-2

this-is-a-job:
  stage: stage-1
```

- Each job **must** have `script` key.
- We can run bash commands under scripts key. They will be executed line by line, same as executing them in a terminal.

```yaml
this-is-a-job:
  stage: stage-1
  script:
      - echo "hello world"
      - pwd
      - ls -la
```

### Default Image

- Gitlab uses ruby image by default, we can change that if we don't need it.
- Anything we specificy at the root level (not under a specific job) will be included to all jobs. In the example below, `this-is-a-job` will use `busybox` and `another-job` will use `ruby:2.7.1`.

```yaml
image: busybox:latest

stages:
  - stage-1
  - stage-2

this-is-a-job:
  stage: stage-1

another-job:
    image: ruby:2.7.1
```

### Variables

- We can store key-value pairs under repo settings. `Settings > CI/CD > Variables`. We can also MASK the variables, so they won't be exposed in the logs. Read more [here](https://docs.gitlab.com/ee/ci/variables/index.html)
- There are also many useful [predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html). You'll need them when building more complex pipelines.

### Artifacts

- Gitlab is cleaning the project directory at the end of a job. At the end of job logs, we can see `Cleaning up project directory and file based variables`.
- Also each job runs in a separate workspace. So, any file created at that job won't be passed to the next stages.
- If we want to pass files between jobs, we must use `artifacts`.

```yaml
job-1:
  stage: stage-1
  script:
    - ...
  artifacts:
    paths:
      - path/we/want/to/preserve
    expire_in: 30 mins
```

- When using `artifacts` at the end of a successful job, artifacts will be uploaded to gitlab. Then downstream jobs will download the artifacts.
- By default, all artifacts from the previous stages will be carried to the next stage. If we want to get only some of the artifacts, the keyword we need is `dependencies`. Read more [here](https://docs.gitlab.com/ee/ci/yaml/#dependencies)
- We can browse or download artifacts if they are not expired yet. By default, GitLab doesn't delete the artifacts from the latest pipeline even if expiration time has passed. This can be changed within Settings > CI/CD > Artifacts.


### Caching

> [Caching in GitLab CI/CD](https://docs.gitlab.com/ee/ci/caching/) looks importants for longer pipelines, haven't tried yet. 


### Runners & Quotas

- [Gitlab Runner](https://docs.gitlab.com/runner/), as the same already tells us, is the application that runs the jobs in GitLab CI/CD pipelines.
- We can have our own Runners installed on some server or we can have use so called Shared Runners. Shared Runners are shared by all users in free tier.
- There is a monthly quota for using sharing runners. By the time I'm writing this the quota is 400 CI/CD minutes per month; more than enough for my side-projects. It's available for all users including private projects.
- We can see the monthly usage across all projects in user settings > usage quotas.
